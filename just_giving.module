<?php
/**
* @file
* A block module that displays data from Just Giving
*/

/**
* Implements hook_help.
*
* Displays help and module information.
*
* @param path
*   Which path of the site we're using to display help
* @param arg
*   Array that holds the current path as returned from arg() function
*/
function just_giving_help($path, $arg) {
  switch ($path) {
    case "admin/help#just_giving":
      return '<p>'.  t("Displays data from Just Giving") .'</p>';
      break;
  }
}

/**
* Implement hook_menu().
*/
function just_giving_menu() {
$items['admin/config/donation/just_giving'] = array(
  'title' => 'Just Giving',
  'description' => 'Configuration for the Just Giving module',
  'page callback' => 'drupal_get_form',
  'page arguments' => array('just_giving_form'),
  'access arguments' => array('administer users'),
  'type' => MENU_NORMAL_ITEM,
);

 return $items;
}

/**
  * Form builder; Create and display the User Warn configuration
  * settings form.
*/
function just_giving_form($form, &$form_state) {
  // Text field for the Just Giving api key
  $form['just_giving_api_key'] = array(
  '#type' => 'textfield',
  '#title' => t('Just Giving api key'),
  '#default_value' => variable_get('just_giving_api_key'),
  '#description' => t('The key for the Just Giving api'),
    '#size' => 40,
    '#maxlength' => 120,
    '#required' => TRUE,
  );
  
  // Text field for the Just Giving page
  $form['just_giving_page'] = array(
  '#type' => 'textfield',
  '#title' => t('Just Giving page'),
  '#default_value' => variable_get('just_giving_page'),
  '#description' => t('The Just Giving page'),
    '#size' => 40,
    '#maxlength' => 120,
    '#required' => TRUE,
  );
  
  // Number of past donations
  $form['just_giving_past_donations'] = array(
  '#type' => 'textfield',
  '#title' => t('Past Donations'),
  '#default_value' => variable_get('just_giving_past_donations'),
  '#description' => t('The number of past donations to display in the widget'),
    '#size' => 40,
    '#maxlength' => 120,
    '#required' => TRUE,
  );

  return system_settings_form($form);
}

function just_giving_form_validate($form, &$form_state) {
  $past_donations = $form_state['values']['just_giving_past_donations'];
  if (!is_numeric($past_donations)) {
    form_set_error('just_giving_past_donations', t('You must enter a number for the amount of past donations'));
  }
}

/**
 * Save configuration settings for Just Giving module.
 */
function just_giving_form_submit($form, &$form_state) {
 variable_set('just_giving_api_key',
            $form_state['values']['just_giving_api_key']);
 variable_set('just_giving_past_donations',
            $form_state['values']['just_giving_past_donations']);
 variable_set('just_giving_page',
            $form_state['values']['just_giving_page']);

 drupal_set_message(t('The settings have been saved'));
}

/**
* Implements hook_block_info().
*/
function just_giving_block_info() {
  $blocks['just_giving'] = array(
    'info' => t('Just Giving'), //The name that will appear in the block list.
    'cache' => DRUPAL_CACHE_PER_ROLE, //Default
  );
  return $blocks;
}

/**
 * Load the Just Giving api
 *
 */

function jgwp_getdonationpage($jgwp_host,$jgwp_jgpage,$jgwp_apikey) {
  $url = $jgwp_host."/".$jgwp_apikey."/v1/fundraising/pages/".$jgwp_jgpage."/donations";
  $str = file_get_contents($url);
  return $str;
}

function jgwp_getpage($jgwp_host,$jgwp_jgpage,$jgwp_apikey) {
  $url = $jgwp_host."/".$jgwp_apikey."/v1/fundraising/pages/".$jgwp_jgpage;
  $str = file_get_contents($url);
  return $str;
}

function jgwp_getdonations($xml,$index) {
  $jgwp_res = new SimpleXMLElement($xml);
  $jgwp_pagesize = $jgwp_res->pagination->pageSizeReturned;
  
  if ($index < $jgwp_pagesize) {
    $jgwp_amount = floatval($jgwp_res->donations->donation[$index]->amount);
    $jgwp_donordisplay = $jgwp_res->donations->donation[$index]->donorDisplayName;
    $jgwp_giftaid = floatval($jgwp_res->donations->donation[$index]->estimatedTaxReclaim);
    $jgwp_message = $jgwp_res->donations->donation[$index]->message;
    $jgwp_str = "<li><p style='color:#3DA530;float:left;width:100%;padding-bottom:0;'><b>".$jgwp_donordisplay." - &pound;".number_format($jgwp_amount,2)."</b></p><p class='speech'>".$jgwp_message."</p></li>";

    return $jgwp_str;
    
  } else {
  
    return $null;
  }
}

function jgwp_getdata($xml,$val) {
  $jgwp_res = new SimpleXMLElement($xml);
  $jgwp_symbol = $jgwp_res->currencySymbol;

  switch ($val):

	  case "online":
	    $jgwp_value = floatval($jgwp_res->totalRaisedOnline);
	  break;

	  case "offline":
	    $jgwp_value = floatval($jgwp_res->totalRaisedOffline);
	  break;

	  case "giftaid":
	    $jgwp_value = floatval($jgwp_res->totalEstimatedGiftAid);
	  break;

	  case "total":
	    $jgwp_value = floatval($jgwp_res->totalRaisedOnline) + floatval($jgwp_res->totalRaisedOffline);
	  break;
	
	  case "goal":
	    $jgwp_value = floatval($jgwp_res->fundraisingTarget);
	  break;

  endswitch;
  
  $jgwp_symbol=htmlentities(utf8_decode($jgwp_symbol));
  $jgwp_value = number_format($jgwp_value,2);
  $jgwp_return = $jgwp_symbol.$jgwp_value;

  return($jgwp_return);
}

function jgwp_getpageid($xml) {
  $jgwp_res = new SimpleXMLElement($xml);
  $jgwp_return = floatval($jgwp_res->pageId);

  return($jgwp_return);
}

/**
 * Implementation of hook_block_view().
 */
function just_giving_block_view($delta='') {
  $block = array();
  switch ($delta) {
    case 'just_giving':
      $block['subject'] = t('Just Giving');
      $block['content'] = just_giving_contents();
      break;
  }
  return $block;
}

function just_giving_contents() {
  $jgwp_host = "https://api.justgiving.com";
  $jgwp_pagehost = "http://www.justgiving.com";
  $data= jgwp_getpage($jgwp_host,variable_get('just_giving_page'),variable_get('just_giving_api_key'));
  $goal = jgwp_getdata($data,'goal');
  $goal_preg = preg_replace("/[^0-9.]*/", "", $goal);
  $total_raised = jgwp_getdata($data,'total');
  $total_raised_preg = preg_replace("/[^0-9.]*/", "", $total_raised);
  $percentage = ($total_raised_preg / $goal_preg) * 100;
  
  $total_online = jgwp_getdata($data,'online');
  $total_online_preg = preg_replace("/[^0-9.]*/", "", $total_online);
  $percentage_online = ($total_online_preg / $goal_preg) * 100;
  
  $total_offline = jgwp_getdata($data,'offline');
  $total_offline_preg = preg_replace("/[^0-9.]*/", "", $total_offline);
  $percentage_offline = ($total_offline_preg / $goal_preg) * 100;
  
  //Progress layout
  $output = "<div id=\"summary\"><div id=\"bar\"><span>Total Progress</span>";
  
  if($percentage >= 100) { 
    $output .= "<div id=\"progress-bar\" class=\"all-rounded\">
    <div id=\"progress-bar-percentage\" class=\"all-rounded\" style=\"width: 100%;background:#3DA530;\">
    <div class=\"spacer\">We've reached our goal! Thank you!</div>
    </div></div>";
  } else {
	  $output .= "<div id=\"progress-bar\" class=\"all-rounded\">";
	    if ($total_raised_preg < 1) { 
	      $output .= "<div id=\"progress-bar-percentage\" class=\"all-rounded\" style=\"width: 100%;background:#A53130;\">
	      <div class=\"spacer\">No sponsors yet!</div>"; 
	    } else {
	      $output .= "<div id=\"progress-bar-percentage\" class=\"all-rounded\" style=\"width: $percentage%\">
		    <span class=\"percentage-text\">$percentage%</span>";
		  }
	  $output .= "</div></div>";
	}
	$output .= "</div>";
	
	
	$output .= "<div id=\"bar\">";
	if($percentage >= 100) { 
	  $output .= "<div id=\"progress-bar\" class=\"all-rounded\" style=\"display:none;\">
    <div id=\"progress-bar-percentage\" class=\"all-rounded\" style=\"width: 100%;background:#3DA530;\">
    <div class=\"spacer\">We've reached our goal! Thank you!</div>
    </div></div>";
  } else {
	  $output .= "<span>Sponsored online</span>
	  <div id=\"progress-bar\" class=\"all-rounded\">";
	    if ($total_online_preg < 1) { 
	      $output .= "<div id=\"progress-bar-percentage\" class=\"all-rounded\" style=\"width: 100%\">
	      <div class=\"spacer\">No online sponsors yet!</div>"; 
	    } else {
	      $output .= "<div id=\"progress-bar-percentage\" class=\"all-rounded\" style=\"width: $percentage_online%\">
		    <span class=\"percentage-text\">$total_online</span>";
		  }
	    $output .= "</div></div>";
	}
	$output .= "</div>";
	
	
	$output .= "<div id=\"bar\">";
	if($percentage >= 100) { 
	  $output .= "<div id=\"progress-bar\" class=\"all-rounded\" style=\"display:none;\">
    <div id=\"progress-bar-percentage\" class=\"all-rounded\" style=\"width: 100%;background:#3DA530;\">
    <div class=\"spacer\">We've reached our goal! Thank you!</div>
    </div></div>";
  } else {
	  $output .= "<span>Sponsored offline</span>
	  <div id=\"progress-bar\" class=\"all-rounded\">";
	    if ($total_offline_preg < 1) { 
	      $output .= "<div id=\"progress-bar-percentage\" class=\"all-rounded\" style=\"width: 100%\">
	      <div class=\"spacer\">No offline sponsors yet!</div>"; 
	    } else {
	      $output .= "<div id=\"progress-bar-percentage\" class=\"all-rounded\" style=\"width: $percentage_offline%\">
		    <span class=\"percentage-text\">$total_offline</span>";
		  }
	  $output .= "</div></div>";
	}
	$output .= "</div>";
	
	$output .= "<div style=\"text-align: right; width: 100%; margin-bottom: 0.5em;\">Total: <b>$total_raised</b>/$goal</div>";
	
	$output .= "</div>";

  $donate_page = variable_get('just_giving_page');
  $output .= "<div id='donate-buttons' style='margin-bottom:1.5em;'>
  <div id='visit' class='button medium'><a class='button blue' style='font-weight: normal;' href='".$jgwp_pagehost."/".$donate_page."'><span>Visit</span></a></div>
  <div id='donate' class='button medium'><a class='button red' href='".$jgwp_pagehost."/donation/sponsor/page/".jgwp_getpageid($data)."?amount=2&exitUrl=".urlencode("http://" . $_SERVER['HTTP_HOST']  . $_SERVER['REQUEST_URI'])."'><span>Sponsor</span></a></div>
  </div>";
   
  $output .= "<h2><span>Recent Sponsors</span></h2>";
  $output .= "<ul>";
  $i=0;
  while ($i<variable_get('just_giving_past_donations')) {
    $jgwp_xml = jgwp_getdonationpage($jgwp_host,variable_get('just_giving_page'),variable_get('just_giving_api_key'));
    $output .= jgwp_getdonations($jgwp_xml,$i);
    $i++;
  }

  $output .= "</ul>";
  return $output;
}

drupal_add_css(drupal_get_path('module', 'just_giving') . '/just_giving.css', array('group' => CSS_DEFAULT, 'type' => 'file'));